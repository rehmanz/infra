package cmd

import (
   "log"
   "github.com/spf13/cobra"
)

var buildCmd = &cobra.Command{
   Use:   "build",
   Short: "Build command",
   Run: func(cmd *cobra.Command, args []string) {
      log.Println("#TODO: Implement build command")
   },
}

var tagCmd = &cobra.Command{
   Use:   "tag",
   Short: "Tag command",
   Run: func(cmd *cobra.Command, args []string) {
      log.Println("#TODO: Implement tag command")
   },
}

func init() {
   rootCmd.AddCommand(buildCmd)
   buildCmd.Flags().StringArray("c", []string{}, "List of components")

   buildCmd.AddCommand(tagCmd)
}
