package cmd

import (
    "os"
    "log"
    "github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
    Use:   "fusion",
    Short: "A software release management tool",
}

func Execute() {
    if err := rootCmd.Execute(); err != nil {
        log.Println(err)
        os.Exit(1)
    }
}