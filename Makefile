export GOPATH=${PWD}
export PATH := bin:$(PATH)

validate:
	which go

clean:
	rm -rf bin/*

compile:
	go get -u github.com/spf13/cobra/cobra
	make -C src/github.com/fusionci/fusion compile

test:
	fusion help
	fusion help build
	fusion build
	fusion build --c foo bar baz
	fusion build tag

.PHONY: validate clean compile test package deploy